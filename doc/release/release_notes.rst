..
    :copyright: Copyright (c) 2016 ftrack

.. _release/release_notes:

*************
Release Notes
*************

.. release:: 0.2.0
    :date: 2016-11-18

    .. change:: new
        :tags: Webpack, Node

        Include a compiled lib folder, so that the package can be imported in
        a webpack or node project.

        .. seealso::

            :ref:`installing`

    .. change:: new
        Added helper method on session for creating and uploading a component
        from a file.

        .. seealso::

            :ref:`Uploading files <tutorial/create_component>`

.. release:: 0.1.0
    :date: 2016-06-13

    .. change:: new

        Initial release with support for query, create, update and delete
        operations.
